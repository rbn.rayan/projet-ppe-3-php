<?php

require './pages_php/db_connection.php';
require './pages_php/functions.php';
session_start();

$_SESSION['currentPage'] = basename(__FILE__);

?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DARK - Accueil</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./styles/default_style.css">
    <link rel="stylesheet" href="./styles/accueil.css">
  </head>
  <body class="text-light">

    <?php require './pages_php/header.php'; ?>

    <!-- Page d'accueil -->

    <!-- Entete -->
    <header class="container-fluid d-flex justify-content-center align-items-center text-center">
      <div class="col-4">
        <img src="./img/trinity-logo 1.svg" alt="Logo radioactif" class="col img-fluid logo">
      </div>

      <h2 class="col-lg-4 col-md-12">Bienvenue dans l'univers de DARK</h2>

      <div class="col-4">
        <img src="./img/trinity-logo 1.svg" alt="Logo radioactif" class="col img-fluid logo">
      </div>
    </header>

    <!-- Conteneur principale -->
    <div class="container main-container">

      <!-- Ligne principale -->
      <div class="row main-row">

        <!-- Contenue principale / centrale (3/4 de l'ecran) -->
        <main class="col-lg-9 col-md-12 text-center">
          <div class="row">
            <h2>Page d'accueil</h2>
            <p>Bienvenue sur la page d'accueil. Ce site est consacré à l'univers de la série Netflix DARK.</p>
          </div>

          <div class="row">
            <h3 class="h4">Le site se construit !</h3>
            <p class="date">28 Mai 2020</p>

            <p>Voilà la toute première mouture du site sur l'univers de la série DARK sur Netflix.</p>

            <img src="./img/logo_dark1.jpg" alt="" class="img-fluid rounded">
          </div>

        </main>

        <!-- Barre laterale (1/4 de l'ecran) -->
        <aside class="col-lg-3 col-md-12 d-flex flex-column align-items-center justify-content-start">

          <div class="row text-center mt-lg-5 mb-lg-4">
            <h4>A propos</h4>
            <p>Ce site est en l'honneur de l'univers de la serie allemande DARK.</p>
          </div>

          <div class="row text-center mt-lg-5 mb-lg-4">
            <h4>Sites favoris :</h4>
            <ul class="d-flex flex-column justify-content-center align-items-center">
              <li><a href="https://fr.wikipedia.org/wiki/Dark_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e)" target="_blank">wikipedia.org/wiki/Dark</a></li>
              <li><a href="https://dark.fandom.com/fr/wiki/Wiki_Dark" target="_blank">dark.fandom.com</a></li>
            </ul>
          </div>

          <div class="row text-center mt-lg-5 mb-lg-4">
            <h4>Trailer :</h4>
            <p><a href="https://www.youtube.com/watch?v=BZ5OCIJVErw" target="_blank">youtube.com</a></p>
          </div>

        </aside>

      </div>

    </div>

    <?php require './pages_php/footer.php'; ?>

    <script src="./js/bootstrap.min.js"></script>

  </body>
</html>
