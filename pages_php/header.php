<?php

if (!exist($_SESSION, ['currentPage'])) {
  die('Page non trouvable');
}

if (!isset($_SESSION['isLogged'])) {
  $_SESSION['isLogged'] = false;
}

?>

<!-- Image en banniere -->
<div class="banner container-fluid">
  <img src="./img/dark-wallpaper.jpg" alt="">
</div>

<!-- Barre de navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark d-flex align-items-center justify-content-between">
  <div class="container-fluid">

    <h1><a class="navbar-brand" href="./index.php">DARK</a></h1>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse w-75" id="navbarNav">
      <ul class="navbar-nav mx-auto">
        <li class="nav-item">
          <a class="nav-link <?php if ($_SESSION['currentPage'] == 'index.php') {
                                echo 'active';
                              } ?>" aria-current="page" href="./index.php">Accueil</a>
        </li>

        <li class="nav-item">
          <a class="nav-link <?php if ($_SESSION['currentPage'] == 'personnages.php' || $_SESSION['currentPage'] == 'details_personnage.php') {
                                echo 'active';
                              } ?>" href="./personnages.php">Personnages</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php if ($_SESSION['currentPage'] == 'familles.php' || $_SESSION['currentPage'] == 'details_famille.php') {
                                echo 'active';
                              } ?>" href="./familles.php">Familles</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php if ($_SESSION['currentPage'] == 'comments.php' || $_SESSION['currentPage'] == 'details_personnage.php') {
                                echo 'active';
                              } ?>" href="./comments.php">Discussion</a>
        </li>
        <li class="nav-connection nav-item d-lg-none d-md-flex">
          <?php if ($_SESSION['isLogged']) : ?>
            <a href="./pages_php/logout.php" class="connection text-light fw-bold">Se deconnecter</a>
          <?php endif; ?>
          <?php if (!$_SESSION['isLogged']) : ?>
            <a href="./connection_form.php" class="connection text-light fw-bold">Connexion</a>
          <?php endif; ?>
        </li>
      </ul>
    </div>

    <div class="nav-connection d-lg-flex flex-column justify-content-start d-none">
      <?php if ($_SESSION['isLogged']) : ?>
        <a href="./pages_php/logout.php" class="connection text-light fw-bold">Se deconnecter</a>
      <?php endif; ?>
      <?php if (!$_SESSION['isLogged']) : ?>
        <a href="./connection_form.php" class="connection text-light fw-bold">Connexion</a>
      <?php endif; ?>
    </div>

  </div>
</nav>
