<?php

session_start();

if ($_SESSION['isLogged']) {
  $_SESSION['isLogged'] = false;
  unset($_SESSION['username']);
  header('Location: ../index.php');
}
