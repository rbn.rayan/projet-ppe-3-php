<?php

/* Fonctions */

function exist($_ARRAY, $values) {
  foreach($values as $value) {
    if(!isset($_ARRAY[$value]) || trim($_ARRAY[$value]) == '') {
      return false;
    }
  }
  return true;
}