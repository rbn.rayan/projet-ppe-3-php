<?php

require './db_connection.php';
require './functions.php';
session_start();

if (!exist($_SESSION, ['currentPage'])) {
  die('Page non trouvable');
}

if (!exist($_POST, ['username', 'password'])) {
  die('Champs manquant.');
}

try {
  $stmt = $dbh->prepare('INSERT INTO utilisateurs (nom, mdp) VALUES (:username, :password)');
  $psswd = md5($_POST['password']);
  $stmt->bindParam(':username', $_POST['username']);
  $stmt->bindParam(':password', $psswd);
  if ($stmt->execute()) {
    header('Location: ../connection_form.php');
  }
} catch (Exception $e) {
  var_dump($e);
}

echo 'Erreur d\'insertion de l\'utilisateur';
