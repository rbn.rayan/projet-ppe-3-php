<?php

require 'db_connection.php';
require 'functions.php';
session_start();

if (!exist($_POST, ['username', 'password']) || !exist($_SESSION, ['currentPage'])) {
  die('Champs manquant.');
}

try {
  $stmt = $dbh->prepare('SELECT nom FROM utilisateurs WHERE nom=:username AND mdp=:password');
  $psswd = md5($_POST['password']);
  $stmt->bindParam(':username', $_POST['username']);
  $stmt->bindParam(':password', $psswd);
  if (!$stmt->execute()) {
    die('Erreur recuperation de l\'utilisateur');
  }
} catch (Exception $e) {
  var_dump($e);
}

$user = $stmt->fetch();

var_dump($user);
if ($user) {
  $_SESSION['isLogged'] = true;
  $_SESSION['username'] = $user['nom'];
  header('Location: ../index.php');
} else {
  header('Location: ../connection_form.php');
}

