<?php

session_start();
require 'functions.php';
require 'db_connection.php';

if (!exist($_SESSION, ['currentPage', 'isLogged', 'username']) || !exist($_GET, ['title', 'comment'])) {
    die('Erreur: champs manquants.');
}

try {
    $stmt = $dbh->prepare('INSERT INTO commentaires (auteur, sujet, commentaire) VALUES (:username, :title, :comment)');
    $stmt->bindParam(':username', $_SESSION['username']);
    $stmt->bindParam(':title', $_GET['title']);
    $stmt->bindParam(':comment', $_GET['comment']);
} catch (Exception $e) {
    var_dump($e);
    die();
}


if ($stmt->execute()) {
    header('Location: ../comments.php');
} else {
    header('Location: ../comment_form.php');
}