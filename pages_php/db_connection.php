<?php

try {
  $options = [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'];
  $dbh = new PDO('mysql:host=localhost;dbname=dark', 'root', null, $options);
} catch(Exception $e) {
  var_dump($e);
  die('Erreur : connexion BDD');
}