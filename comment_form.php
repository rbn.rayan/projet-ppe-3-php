<?php

require './pages_php/functions.php';
session_start();

if (!exist($_SESSION, ['currentPage'])) {
  die('Page non trouvable');
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Commentaire</title>
  <link rel="stylesheet" href="./css/bootstrap.min.css">
  <link rel="stylesheet" href="./styles/default_style.css">
  <link rel="stylesheet" href="./styles/personnages_familles.css">
  <link rel="stylesheet" href="./styles/connection_form.css">
</head>

<body class="text-light">

  <?php require './pages_php/header.php'; ?>

  <div class="container main-container">

    <main class="main-row row w-50 mx-auto">
      <h2 class="text-center mb-5 mt-2">Connexion</h2>
      <form action="./pages_php/add_comment.php" method="GET" class="d-flex flex-column mx-auto mb-2 w-75">
        <label for="title" class="form-label">Sujet :</label>
        <input type="text" id="title" name="title" class="form-control mb-2" required>

        <br>

        <label for="comment" class="form-label">Commentaire :</label>
        <textarea name="comment" id="comment" cols="30" rows="10" class="mb-5"></textarea>

        <input type="submit" value="Ajouter le commentaire" class="btn bg-dark text-light">
      </form>

    </main>

  </div>

  <?php require './pages_php/footer.php'; ?>

  <script src="./js/bootstrap.min.js"></script>

</body>

</html>
