-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 29 avr. 2021 à 15:49
-- Version du serveur :  10.4.13-MariaDB
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `dark`
--

-- --------------------------------------------------------

--
-- Structure de la table `familles`
--

DROP TABLE IF EXISTS `familles`;
CREATE TABLE IF NOT EXISTS `familles` (
  `code_famille` varchar(2) NOT NULL,
  `nom_famille` varchar(20) NOT NULL,
  `desc_famille` varchar(150) DEFAULT NULL,
  `pht_Famille` varchar(40) NOT NULL,
  PRIMARY KEY (`code_famille`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `familles`
--

INSERT INTO `familles` (`code_famille`, `nom_famille`, `desc_famille`, `pht_Famille`) VALUES
('DO', 'Doppler', 'La famille Doppler est l\'une des quatre familles interconnectées dont les membres sont les personnages centraux de Dark.', 'Doppler-family.jpg'),
('KA', 'Kahnwald', 'La famille Kahnwald est l\'une des quatres familles interconnectées qui sont au centre du déroulement de la série (notament Jonas).', 'Kahnwald-family.jpg'),
('NI', 'Nielsen', 'La famille Nielsen est l’une des quatre familles interconnectées dont les membres sont les personnages centraux de Dark.', 'Nielsen-family.jpg'),
('TI', 'Tiedemann', 'La famille Tiedemann est l\'une des quatre familles interconnectées dont les membres sont les personnages centraux de Dark.', 'Tiedemann-family.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `personnages`
--

DROP TABLE IF EXISTS `personnages`;
CREATE TABLE IF NOT EXISTS `personnages` (
  `code_personnage` int(11) NOT NULL AUTO_INCREMENT,
  `nom_personnage` varchar(20) NOT NULL,
  `prenom_personnage` varchar(20) NOT NULL,
  `annee_naissance_personnage` int(4) NOT NULL,
  `occupation_personnage` varchar(35) NOT NULL,
  `genre_personnage` varchar(1) NOT NULL,
  `alias_personnage` varchar(25) DEFAULT NULL,
  `desc_personnage` text DEFAULT NULL,
  `code_famille` varchar(2) NOT NULL,
  `pht_personnage` varchar(30) DEFAULT NULL,
  `pht_details_personnage` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`code_personnage`),
  KEY `Fk_code_famille` (`code_famille`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `personnages`
--

INSERT INTO `personnages` (`code_personnage`, `nom_personnage`, `prenom_personnage`, `annee_naissance_personnage`, `occupation_personnage`, `genre_personnage`, `alias_personnage`, `desc_personnage`, `code_famille`, `pht_personnage`, `pht_details_personnage`) VALUES
(1, 'Kahnwald', 'Jonas', 2003, 'Lycéen', 'M', 'L\'étranger / Adam,', 'Il est le fils d\'Hannah et de Michael Kahnwald.Suite au suicide de son père, Jonas a passé deux mois en hôpital psychiatrique principalement en raison d’hallucinations et de cauchemars dans lesquels il voyait son père. Pour éviter toutes gênes, son ami Bartosz Tiedemann dira à ses camarades que Jonas était en France (en Italie dans la version française). <br> <br>\r\n\r\nDe retour à Winden, après son séjour à l’hôpital psychiatrique, Jonas se plaindra à son psychiatre Peter Doppler d’avoir encore des visions de son père malgré son traitement. En fouillant la pièce dans laquelle son père s’est suicidé, le jeune garçon découvrira une carte des grottes de Winden cachée dans le plafond.', 'KA', 'Jonas-Kahnwald.jpg', 'details-Jonas.jpg'),
(2, 'Kahnwald', 'Hannah', 1972, 'Physiothérapeute', 'F', 'Aucun', 'Hannah est la mère de Jonas Kahnwald, la veuve de Michael Kahnwald et la maîtresse d\'Ulrich Nielsen. <br> <br>\r\n\r\nOn le retrouve en 1986, où elle est amie avec Katharina et Ulrich et secrètement amoureuse de ce dernier. Elle rencontre Mikkel Nielsen, qui prend le nom de Michael Kahnwald, et l’épouse. ', 'KA', 'Hannah-Kahnwald.jpg', 'details-Hannah.jpg'),
(3, 'Kahnwald', 'Michael', 2008, 'Peintre', 'M', 'Mikkel Nielsen', 'Mikkel est le fils d\'Ulrich Nielsen et Katharina Nielsen et le frère de Magnus et Martha. Il a 12 ans au début de la série et il disparaît alors qu\'il accompagne son frère, sa sœur, Jonas et Bartosz dans la forêt. Il adore la magie. <br> <br>\r\n\r\nOn le retrouve en 1986, où il est recueilli et finalement adopté par Ines Kahnwald et prend le nom de Michael Kahnwald. Il y rencontre Hannah Kahnwald, avec laquelle il se marie et a un fils, Jonas Kahnwald. Il se suicide en 2019, peu avant les événements de la série, et laisse une lettre à son fils dans laquelle il se confie. ', 'KA', 'Michael-Kahnwald.jpg', 'details-Michael.jpg'),
(4, 'Kahnwald', 'Ines', 1940, 'Infirmiere', 'F', 'Aucun', 'Description Ines', 'KA', 'Ines-Kahnwald.jpg', 'details-Ines.jpg'),
(5, 'Kahnwald', 'Daniel', 1915, 'Policier', 'M', 'Aucun', 'Description Daniel', 'KA', 'Daniel-Kahnwald.jpg', 'details-Daniel.jpg'),
(6, 'Tiedemann', 'Claudia', 1942, 'ex PDG centrale nucléaire', 'F', 'Le démon blanc', 'Description Claudia', 'TI', 'Claudia-Tiedemann.jpg', 'details-Claudia.jpg'),
(7, 'Tiedemann', 'Regina', 1971, 'Gérante Hotel', 'F', 'Aucun', 'Description Regina', 'TI', 'Regina-Tiedemann.jpg', 'details-Regina.jpg'),
(8, 'Tiedemann', 'Aleksander', 1970, 'PDG centrale nucléaire', 'M', 'Boris Niewald', 'Description Aleksander', 'TI', 'Aleksander-Tiedemann.jpg', 'details-Aleksander.jpg'),
(9, 'Tiedemann', 'Bartosz', 2003, 'Lycéen', 'M', 'Aucun', 'Description Bartosz', 'TI', 'Bartosz-Tiedemann.jpg', 'details-Bartosz.jpg'),
(10, 'Tiedemann', 'Hanno', 1904, 'Membre du Sic mundus', 'M', 'Noah', 'Description Hanno', 'TI', 'Hanno-Tiedemann.jpg', 'details-Hanno.jpg'),
(11, 'Tiedemann', 'Silja', 1988, 'Interpréte d\'Elizabeth Doppler', 'F', 'Aucun', 'Description Silja', 'TI', 'Silja-Tiedemann.jpg', 'details-Silja.jpg'),
(12, 'Doppler', 'Elisabeth', 2011, 'Leader du Sic mundus', 'F', 'Aucun', 'Description Elisabeth', 'DO', 'Elisabeth-Doppler.jpg', 'details-Elisabeth.jpg'),
(13, 'Doppler', 'Helge', 1944, 'Homme de ménage', 'M', 'Aucun', 'Description Helge', 'DO', 'Helge-Doppler.jpg', 'details-Helge.jpg'),
(14, 'Doppler', 'Franziska', 2003, 'Lycéenne', 'F', 'Aucun', 'Description Franziska', 'DO', 'Franziska-Doppler.jpg', 'details-Franziska.jpg'),
(15, 'Doppler', 'Bernd', 1913, 'ex PDG centrale nucléaire', 'M', 'Aucun', 'Description Bernd', 'DO', 'Bernd-Doppler.jpg', 'details-Bernd.jpg'),
(16, 'Doppler', 'Charlotte', 2041, 'Chef de la police', 'F', 'Aucun', 'Description Charlotte', 'DO', 'Charlotte-Doppler.jpg', 'details-Charlotte.jpg'),
(17, 'Doppler', 'Peter', 1970, 'Psychologue', 'M', 'Aucun', 'Description Peter', 'DO', 'Peter-Doppler.jpg', 'details-Peter.jpg'),
(18, 'Nielsen', 'Magnus', 2001, 'Lycéen', 'M', 'Aucun', 'Description Magnus', 'NI', 'Magnus-Nielsen.jpg', 'details-Magnus.jpg'),
(19, 'Nielsen', 'Katharina', 1970, 'Directrice du Lycée', 'F', 'Aucun', 'Description Katharina', 'NI', 'Katharina-Nielsen.jpg', 'details-Katharina.jpg'),
(20, 'Nielsen', 'Ulrich', 1970, 'Policier', 'M', 'Le commissaire', 'Description Ulrich', 'NI', 'Ulrich-Nielsen.jpg', 'details-Ulrich.jpg'),
(21, 'Nielsen', 'Martha', 2003, 'Lycéenne', 'F', 'Eva', 'Description Martha', 'NI', 'Martha-Nielsen.jpg', 'details-Martha.jpg'),
(22, 'Nielsen', 'Mads', 1973, 'Ecolier', 'M', 'Aucun', 'Description Mads', 'NI', 'Mads-Nielsen.jpg', 'details-Mads.jpg'),
(23, 'Nielsen', 'Tronte', 1941, 'Journaliste', 'M', 'Aucun', 'Description Tronte', 'NI', 'Tronte-Nielsen.jpg', 'details-Tronte.jpg'),
(24, 'Nielsen', 'Agnes', 1910, 'Membre du Sic mundus', 'F', 'Aucun', 'Description Agnes', 'NI', 'Agnes-Nielsen.jpg', 'details-Agnes.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `nom` varchar(25) NOT NULL,
  `mdp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`nom`, `mdp`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `personnages`
--
ALTER TABLE `personnages`
  ADD CONSTRAINT `Fk_code_famille` FOREIGN KEY (`code_famille`) REFERENCES `familles` (`code_famille`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
