<?php 

require './pages_php/db_connection.php';
require './pages_php/functions.php';
session_start();

$_SESSION['currentPage'] = basename(__FILE__);

try {
	$characters = $dbh->query('SELECT code_personnage, nom_personnage, prenom_personnage, pht_personnage FROM personnages');
	$characters = $characters->fetchAll(PDO::FETCH_ASSOC);
} catch(Exception $e) {
	var_dump($e);
	die('Erreur recuperation des données.');
}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>DARK - Personnages</title>
		<link rel="stylesheet" href="./css/bootstrap.min.css">
		<link rel="stylesheet" href="./styles/default_style.css">
		<link rel="stylesheet" href="./styles/personnages_familles.css">
	</head>
	<body class="text-light">

		<?php require './pages_php/header.php'; ?>

		<!-- Conteneur principale -->
		<div class="container main-container">

			<!-- Ligne principale -->
			<div class="row main-row">

				<!-- Contenue principal / personnages -->
				<main class="col-lg-9 col-md-12">

					<div class="row text-center">
						<h2>Personnages</h2>
					</div>
					
					<!-- Conteneur de personnages -->
					<div class="row">

						<?php foreach($characters as $character): ?>

						<div class="col-lg-4 col-md-6 text-center character-container">
							<a href="./details_personnage.php?codePersonnage=<?= $character['code_personnage']; ?>" class="mx-auto">
								<img src="./img/personnages/<?= $character['pht_personnage']; ?>" alt="" class="img-fluid rounded">
								<p><?= $character['nom_personnage'].' '.$character['prenom_personnage']; ?></p>
							</a>
						</div>

						<?php endforeach ?>

					</div>

				</main>

				<!-- Barre laterale (1/4 de l'ecran) -->
        <aside class="col-lg-3 col-md-12 d-flex flex-column align-items-center justify-content-start">

          <div class="row text-center mt-lg-5 mb-lg-4">
            <h4>A propos</h4>
            <p>Ce site est en l'honneur de l'univers de la serie allemande DARK.</p>
          </div>

          <div class="row text-center mt-lg-5 mb-lg-4">
            <h4>Sites favoris :</h4>
            <ul class="d-flex flex-column justify-content-center align-items-center">
              <li><a href="https://fr.wikipedia.org/wiki/Dark_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e)" target="_blank">https://fr.wikipedia/org</a></li>
              <li><a href="https://dark.fandom.com/fr/wiki/Wiki_Dark" target="_blank">https://dark.fandom.com</a></li>
            </ul>
          </div>

          <div class="row text-center mt-lg-5 mb-lg-4">
            <h4>Trailer :</h4>
            <p><a href="https://www.youtube.com/watch?v=BZ5OCIJVErw" target="_blank">youtube.com</a></p>
					</div>
					
        </aside>

			</div>

		</div>

		<?php require './pages_php/footer.php'; ?>

		<script src="./js/bootstrap.min.js"></script>

	</body>
</html>
