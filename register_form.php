<?php

require './pages_php/functions.php';
session_start();

if (!exist($_SESSION, ['currentPage'])) {
  die('Page non trouvable');
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>S'inscrire</title>
  <link rel="stylesheet" href="./css/bootstrap.min.css">
  <link rel="stylesheet" href="./styles/default_style.css">
  <link rel="stylesheet" href="./styles/personnages_familles.css">
  <link rel="stylesheet" href="./styles/connection_form.css">
</head>

<body class="text-light">

  <?php require './pages_php/header.php'; ?>

  <div class="container main-container">

    <main class="main-row row w-50 mx-auto">
      <h2 class="text-center mb-5 mt-2">S'incrire</h2>
      <form action="./pages_php/register.php" method="POST" class="d-flex flex-column mx-auto mb-2 w-75">
        <label for="username" class="form-label">Nom d'utilisateur :</label>
        <input type="text" id="username" name="username" class="form-control mb-5" required>

        <br>

        <label for="password" class="form-label">Mot de passe :</label>
        <input type="password" id="password" name="password" class="form-control mb-5" required>

        <div class="d-flex justify-content-around mb-2">
          <input type="submit" value="S'incrire" class="btn bg-dark text-light px-auto">
        </div>
      </form>

    </main>

  </div>

  <?php require './pages_php/footer.php'; ?>

  <script src="./js/bootstrap.min.js"></script>

</body>

</html>
