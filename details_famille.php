﻿<?php

require './pages_php/db_connection.php';
require './pages_php/functions.php';
session_start();

$_SESSION['currentPage'] = basename(__FILE__);

if(exist($_GET, ['codeFamille'])) {
	try {
		$family = $dbh->prepare('SELECT nom_famille, desc_famille FROM familles WHERE code_famille = :codeFamille');
		$family->bindParam(':codeFamille', $_GET['codeFamille']);
		if(!$family->execute()) {
			die('Erreur.');
		}
		$family = $family->fetch(PDO::FETCH_ASSOC);

		$characters = $dbh->prepare('SELECT code_personnage, nom_personnage, prenom_personnage, pht_personnage FROM personnages WHERE code_famille = :codeFamille');
		$characters->bindParam(':codeFamille', $_GET['codeFamille']);
		if(!$characters->execute()) {
			die('Erreur.');
		}
		$characters = $characters->fetchAll(PDO::FETCH_ASSOC);
	} catch(Exception $e) {
		var_dump($e);
		die('Erreur recuperation des données.');
	}
} else {
	die('Champs codeFamille manquant.');
}

?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>DARK - Famille <?= $family['nom_famille']; ?></title>
		<link rel="stylesheet" href="./css/bootstrap.min.css">
		<link rel="stylesheet" href="./styles/default_style.css">
    <link rel="stylesheet" href="./styles/personnages_familles.css">
    <link rel="stylesheet" href="./styles/details_familles.css">
	</head>
	<body class="text-light">

		<?php require './pages_php/header.php' ?>

		<!-- Conteneur principale -->
		<div class="container main-container">

			<!-- Ligne principale -->
			<main class="row main-row">

        <!-- Contenue principal / personnages -->

        <div class="row text-center">
          <h2>Famille <?= $family['nom_famille']; ?></h2>
          
          <p><?= $family['desc_famille']; ?></p>
        </div>
        
        <!-- Conteneur des Familles -->
        <div class="row">

					<?php foreach($characters as $character): ?>

          <div class="col-lg-4 col-md-6 text-center character-container">
            <a href="./details_personnage.php?codePersonnage=<?= $character['code_personnage'] ?>" class="mx-auto">
              <img src="./img/personnages/<?= $character['pht_personnage']; ?>" alt="" class="img-fluid rounded">
              <p><?= $character['nom_personnage'].' '.$character['prenom_personnage']; ?></p>
            </a>
					</div>
					
					<?php endforeach; ?>

        </div>

      </main>

		</div>

		<?php require './pages_php/footer.php'; ?>

		<script src="./js/bootstrap.min.js"></script>
	</body>
</html>
