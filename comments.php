<?php

require './pages_php/db_connection.php';
require './pages_php/functions.php';
session_start();

$_SESSION['currentPage'] = basename(__FILE__);
$stmt = $dbh->query('SELECT * FROM commentaires');
$comments = $stmt->fetchAll();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Discussion</title>
  <link rel="stylesheet" href="./css/bootstrap.min.css">
  <link rel="stylesheet" href="./styles/default_style.css">
  <link rel="stylesheet" href="./styles/comments.css">
</head>

<body class="text-light">

  <?php require './pages_php/header.php'; ?>

  <!-- Conteneur principale -->
  <div class="container main-container">

    <!-- Ligne principale -->
    <main class="row main-row">

      <div class="row">
        <h2 class="text-center">Discussions</h2>
        <?php if (!$_SESSION['isLogged']) : ?>

          <p>Vous devez être connecter pour poster un message : <a href="./connection_form.php">se connecter</a></p>

        <?php endif; ?>
        <?php if ($_SESSION['isLogged']) : ?>
          <p>Connecter en tant que <b><?= $_SESSION['username'] ?></b> : <a href="./comment_form.php">Ajouter un commentaire</a></p>
        <?php endif; ?>

        <hr>

        <div class="comments">
          <?php foreach($comments as $comment): ?>
          <div class="comment">
            <h2 class="mt-5 mb-0"><?= $comment['sujet']; ?></h2>
            <h4 class="mt-1 mb-3 h6">Auteur : <b><?= $comment['auteur']; ?></b></h4>
            <p>
              <?= $comment['commentaire']; ?>
            </p>
            <hr class="mb-5 mt-5">
          </div>
          <?php endforeach; ?>
        </div>
      </div>

    </main>

  </div>

  <?php require './pages_php/footer.php'; ?>

  <script src="./js/bootstrap.min.js"></script>
</body>

</html>
