﻿<?php

require './pages_php/db_connection.php';
require './pages_php/functions.php';
session_start();

$_SESSION['currentPage'] = basename(__FILE__);

if(exist($_GET, ['codePersonnage'])) {
  try {
    $character = $dbh->prepare('SELECT nom_personnage, prenom_personnage, annee_naissance_personnage, occupation_personnage, genre_personnage, alias_personnage, desc_personnage, code_famille, pht_details_personnage FROM personnages WHERE code_personnage = :codePersonnage');
    $character->bindParam(':codePersonnage', $_GET['codePersonnage']);

    if(!$character->execute()) {
      die('Erreur de recuperation des données.');
    }
    $character = $character->fetch(PDO::FETCH_ASSOC);
  } catch(Exception $e) {
    var_dump($e);
    die('Erreur');
  }
} else {
  die('Erreur : champ codePersonnage manquant.');
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DARK - <?= $character['nom_personnage'].' '.$character['prenom_personnage']; ?></title>
  <link rel="stylesheet" href="./css/bootstrap.min.css">
  <link rel="stylesheet" href="./styles/default_style.css">
  <link rel="stylesheet" href="./styles/details_personnage.css">
</head>
<body class="text-light">

  <?php require './pages_php/header.php'; ?>

  <!-- Conteneur principale -->
  <div class="container main-container">

    <!-- Ligne principale -->
    <main class="row main-row">

      <!-- Image du personnage -->
      <div class="text-center character-img  col-lg-4 col-md-12">
        <img src="./img/details_personnages/<?= $character['pht_details_personnage']; ?>" alt="image du personnage" class="img-fluid rounded">
      </div>

      <div class="col-lg-8 col-md-12 d-flex flex-column justify-content-evenly">

        <!-- Tableau des infos du personnages -->
        <table class="table">
          <thead>
            <tr>
              <th><?= $character['nom_personnage'].' '.$character['prenom_personnage']; ?></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Année de naissance : </th>
              <td><?= $character['annee_naissance_personnage']; ?></td>
            </tr>
            <tr>
              <th>Occupation : </th>
              <td><?= $character['occupation_personnage']; ?></td>
            </tr>
            <tr>
              <th>Alias : </th>
              <td><?= $character['alias_personnage']; ?></td>
            </tr>
            <tr>
              <th>Genre : </th>
              <td><?= $character['genre_personnage']; ?></td>
            </tr>
            <tr>
              <th>Famille : </th>
              <td><a href="./details_famille.php?codeFamille=<?= $character['code_famille']; ?>"><?= $character['nom_personnage']; ?></a></td>
            </tr>
          </tbody>
        </table>

        <!-- Description du personnage -->
        <p><?= $character['desc_personnage']; ?></p>

      </div>

    </main>

  </div>

  <?php require './pages_php/footer.php'; ?>

  <script src="./js/bootstrap.min.js"></script>

</body>
</html>
