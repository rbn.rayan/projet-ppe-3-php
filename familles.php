﻿<?php

require './pages_php/db_connection.php';
require './pages_php/functions.php';
session_start();

$_SESSION['currentPage'] = basename(__FILE__);

try {
  $families = $dbh->query('SELECT code_famille, nom_famille, pht_famille FROM familles');
  $families = $families->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
  var_dump($e);
  die('Erreur recuperation des données.');
}

if (!count($families)) {
  die('Erreur aucune famille.');
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DARK - Familles</title>
  <link rel="stylesheet" href="./css/bootstrap.min.css">
  <link rel="stylesheet" href="./styles/default_style.css">
  <link rel="stylesheet" href="./styles/familles.css">
</head>

<body class="text-light">

  <?php require './pages_php/header.php'; ?>

  <!-- Conteneur principale -->
  <div class="container main-container">

    <!-- Ligne principale -->
    <main class="row main-row">

      <div class="row text-center">
        <h2>Familles</h2>
      </div>

      <div class="row">

        <?php foreach ($families as $family) : ?>

          <div class="col-lg-6 col-md-12 d-flex flex-column align-items-center justify-content-evenly">
            <div class="card">
              <img class="card-img-top rounded" src="./img/familles/<?= $family['pht_famille']; ?>" alt="image famille <?= $family['nom_famille']; ?>">
              <div class="card-body text-center">
                <h5 class="card-title">Famille <?= $family['nom_famille']; ?></h5>
                <p class="card-text">Différents personnages de la famille <?= $family['nom_famille']; ?>.</p>
                <a href="./details_famille.php?codeFamille=<?= $family['code_famille']; ?>" class="btn text-light bg-dark">Détails de la famille <?= $family['nom_famille']; ?></a>
              </div>
            </div>
          </div>

        <?php endforeach; ?>

      </div>

    </main>

  </div>

  <?php require './pages_php/footer.php'; ?>

  <script src="./js/bootstrap.min.js"></script>
</body>

</html>
